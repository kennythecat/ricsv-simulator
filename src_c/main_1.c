#include <stdio.h>
#include <assert.h>

struct RVCore {
    unsigned int pc;
    unsigned int regs[32];
};

void RVCore_step(struct RVCore *self, unsigned int inst) {
    printf("PC = %u, inst = %u\n", self->pc, inst);
    self->pc += 4;
}

void RVCore_run(struct RVCore *self, int num_steps) {
    int step_count = 0;
    while (step_count < num_steps) {
        RVCore_step(self, 0);
        step_count++;
    }
}

int main() {
    struct RVCore core;
    core.pc = 0;
    assert(core.pc == 0);
    RVCore_run(&core, 5);

    // Testing the run function
    assert(core.pc == 20);
    
    return 0;
}