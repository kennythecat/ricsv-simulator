#include "memory_interface.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Node {
    uint32_t key;
    uint8_t value[BLOCK_SIZE];
    struct Node* next;
} Node;

typedef struct {
    Node* head;
    MemoryInterface interface;
} MemoryModel;

// Initialize a new MemoryModel
void new_MemoryModel(MemoryModel *model) {
    model->head = NULL;
    model->interface.access_memory = (void (*)(void*, Payload*)) access_memory;
}

Node* find_or_create_node(MemoryModel *model, uint32_t block_base) {
    // Your logic to find or create a node, initializing it with zeros if needed.
    // Return the found or created node.
    return NULL; // Placeholder
}

uint8_t read_byte(MemoryModel *model, uint32_t addr) {
    uint32_t block_base = addr & 0xffffffe0;
    uint32_t block_offset = addr - block_base;
    Node *node = find_or_create_node(model, block_base);
    return node->value[block_offset];
}

void write_byte(MemoryModel *model, uint32_t addr, uint8_t value) {
    uint32_t block_base = addr & 0xffffffe0;
    uint32_t block_offset = addr - block_base;
    Node *node = find_or_create_node(model, block_base);
    node->value[block_offset] = value;
}

void access_memory(MemoryModel *self, Payload *payload) {
    switch (payload->op) {
        case READ:
            for (size_t i = 0; i < payload->data_len; ++i) {
                payload->data[i] = read_byte(self, payload->addr + i);
            }
            break;
        case WRITE:
            for (size_t i = 0; i < payload->data_len; ++i) {
                write_byte(self, payload->addr + i, payload->data[i]);
            }
            break;
        case INVALID:
            fprintf(stderr, "Invalid memory operation.\n");
            exit(EXIT_FAILURE);
    }
}
