#ifndef MEMORY_INTERFACE_H
#define MEMORY_INTERFACE_H

#include "memory_model.h"

struct RVCore;

void memory_interface_bind_mem(struct RVCore *core, struct MemoryModel *mem);

#endif
