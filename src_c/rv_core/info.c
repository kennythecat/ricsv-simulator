typedef enum {
    AUIPC,
    ADDI,
    C_ADDI,
    C_SWSP,
    C_LWSP,
    NOP,
} InstID;

typedef struct {
    const char* name;
} InstInfo;

static const InstInfo inst_info_table[] = {
    { "auipc" },
    { "addi" },
    { "c_addi" },
    { "c_swsp" },
    { "c_lwsp" },
    { "nop" },
};
