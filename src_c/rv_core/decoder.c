#include <stdio.h>
#include <stdlib.h>
#include "inst_decoder.h"

void decode_inst_compressed(unsigned int inst_bytes, InstType* inst);
void decode_inst_4byte(unsigned int inst_bytes, InstType* inst);

InstType decode(InstDecoder* self, unsigned int inst_bytes) {
    InstType new_inst;
    new_inst.data = inst_bytes;
    new_inst.len = 0;
    new_inst.id = NOP;

    switch (inst_bytes & 0b11) {
        case 0:
        case 1:
        case 2:
            decode_inst_compressed(inst_bytes, &new_inst);
            new_inst.len = 2;
            break;
        default:
            decode_inst_4byte(inst_bytes, &new_inst);
            new_inst.len = 4;
            break;
    }

    return new_inst;
}

void decode_inst_compressed(unsigned int inst_bytes, InstType* inst) {
    unsigned int opcode = inst_bytes & 0x3;
    unsigned int funct3 = (inst_bytes >> 13) & 0x7;
    switch (opcode) {
        case 0x1:
            switch (funct3) {
                case 0x0:
                    inst->id = ADDI;
                    break;
                default:
                    printf("Invalid instruction\n");
                    exit(EXIT_FAILURE);
            }
            break;
        case 0x2:
            switch (funct3) {
                case 0x6:
                    inst->id = C_SWSP;
                    break;
                case 0x2:
                    inst->id = C_LWSP;
                    break;
                default:
                    printf("Invalid instruction\n");
                    exit(EXIT_FAILURE);
            }
            break;
        default:
            printf("Invalid instruction\n");
            exit(EXIT_FAILURE);
    }
}

void decode_inst_4byte(unsigned int inst_bytes, InstType* inst) {
    unsigned int opcode = inst_bytes & 0x7f;
    unsigned int funct3 = (inst_bytes & 0x00007000) >> 12;
    switch (opcode) {
        case 0x13:
            switch (funct3) {
                case 0x0:
                    inst->id = ADDI;
                    break;
                default:
                    printf("Invalid instruction\n");
                    exit(EXIT_FAILURE);
            }
            break;
        case 0x17:
            inst->id = AUIPC;
            break;
        default:
            printf("Invalid instruction\n");
            exit(EXIT_FAILURE);
    }
}
