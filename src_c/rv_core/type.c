#include "info.h"

typedef struct {
    unsigned int data;
    unsigned int len;
    InstID id;
} InstType;

unsigned int get_rd(InstType* self) {
    return (self->data >> 7) & 0x1F;
}

unsigned int get_rs1(InstType* self) {
    return ((self->data & 0x000F8000) >> 15);
}

// Add remaining functions similar to get_rd, get_rs1...

InstType inst_auipc_code(unsigned int rd, unsigned int imm);
InstType inst_addi_code(unsigned int rd, unsigned int rs1, unsigned int imm);

// Add other inst_xxx_code functions...
