#ifndef MEMORY_INTERFACE_H
#define MEMORY_INTERFACE_H

#include <stdint.h>
#include <stdlib.h>

typedef enum {
    READ,
    WRITE,
    INVALID
} MemoryOperation;

typedef struct {
    uint32_t addr;
    uint8_t *data;
    size_t data_len;
    MemoryOperation op;
} Payload;

typedef struct {
    void (*access_memory)(void *self, Payload *payload);
} MemoryInterface;

#endif // MEMORY_INTERFACE_H
