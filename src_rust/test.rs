#[derive(Default)]
pub struct RVCore {
    pc: u32,
    regs: [u32; 32],
}

pub enum InstID {
    C_ADDI,
}

pub struct InstType {
    pub data: u32, // 從memory fetch到的指令
    pub len: u32, // 指令長度
    pub id: InstID, // 指令ID，由decoder填上
}

impl RVCore {
    fn inst_c_addi(&mut self, inst: &inst_type::InstType) {
        if inst.get_rd() != 0 {
           self.regs[inst.get_rd()] = self.regs[inst.get_rd()] + inst.get_imm_ci();
        }
    }      
    fn step(&mut self, inst: u32) {
        println!("PC = {}, inst = {}", self.pc, inst);
        self.pc += 4;
    }
    
    pub fn run(&mut self, num_steps: i32) {
        let mut step_count = 0;
        while step_count < num_steps {
            self.step(2);
            step_count += 1;
        }
    }
}

fn main() {
    let mut core: RVCore = Default::default();
    core.run(5);
}

fn inst_c_addi_code(rd: u32, imm: u32) -> InstType {
    InstType {
        data: (((imm >> 5) & 1) << 12) | (rd << 7) | ((imm & 0x1f) << 2) | 0x1,
        len: 2,
        id: InstID::C_ADDI,
    }
}

#[test]
fn test_inst_c_addi() {
    let mut core: RVCore = Default::default();
    core.regs[2] = 0x1234;
    core.inst_c_addi(&inst_c_addi_code(2, 0x1));
    assert_eq!(0x1235, core.regs[2]);
}